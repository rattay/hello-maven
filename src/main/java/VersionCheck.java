import org.apache.commons.lang3.JavaVersion;
import org.apache.commons.lang3.SystemUtils;

import java.util.Locale;

public class VersionCheck {

    public static boolean isValidJavaVersion(){
        return SystemUtils.isJavaVersionAtLeast(JavaVersion.JAVA_17);
    }

    public static String getCurrentJavaVersion(){
        return System.getProperty("java.version");
    }

    public static JavaVersion getMinJavaVersion() {
        return JavaVersion.JAVA_17;
    }


}
