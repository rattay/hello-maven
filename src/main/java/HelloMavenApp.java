import com.google.common.math.BigIntegerMath;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;


public class HelloMavenApp {

    public static void main(String[] args) throws IOException {
        if(!VersionCheck.isValidJavaVersion()){
            System.err.println("Java version incorrect! Java version " + VersionCheck.getCurrentJavaVersion() + " is to low! Minimum use: Java" + VersionCheck.getMinJavaVersion());
            System.exit(-1);
        }
        System.out.println("Hello Maven");
        Reader in = new FileReader("kundendaten.csv");
        Iterable<CSVRecord> records = CSVFormat.newFormat(';').parse(in);
        for (CSVRecord record : records) {
            String columnNo = record.get(0);
            System.out.print(columnNo + " | ");
            String firstName = record.get(1);
            System.out.print(firstName + " | ");
            String lastName = record.get(2);
            System.out.println(lastName + " | ");
        }

        for (int i = 1; i <= 10; i++) {
            System.out.println("Factorial von " + i + " : " + BigIntegerMath.factorial(i));
        }
    }
}
